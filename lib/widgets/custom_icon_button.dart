import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  CustomIconButton({
    required this.iconPath,
    required this.withBackground,
    this.color,
  });

  final String iconPath;
  final bool withBackground;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    if (withBackground == false)
      return InkWell(
        onTap: () => Navigator.pushNamed(context, '/calendar'),
        child: Image.asset(
          iconPath,
          width: 50,
          height: 50,
        ),
      );
    else
      return InkWell(
        onTap: () => Navigator.pushNamed(context, '/add-time-tracking'),
        child: Container(
          width: 42,
          height: 42,
          margin: const EdgeInsets.only(right: 28),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color,
          ),
          child: Image.asset(
            iconPath,
            width: 50,
            height: 50,
          ),
        ),
      );
  }
}
