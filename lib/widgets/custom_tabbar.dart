import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class CustomTabBar extends StatelessWidget {
  CustomTabBar({
    required this.tabTitles,
    required this.controller,
  });

  final List<String> tabTitles;
  final TabController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(18, 16, 18, 0),
      child: TabBar(
        controller: controller,
        indicator: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/line_background.png'),
            repeat: ImageRepeat.repeatX,
          ),
          border: Border(
            bottom: BorderSide(
              width: 3,
              color: Color.fromRGBO(132, 101, 255, 1),
            ),
          ),
        ),
        labelPadding: const EdgeInsets.all(1),
        labelStyle: textTheme.headline2!.copyWith(fontSize: 18),
        labelColor: Colors.black,
        tabs: tabTitles.map((title) => Tab(text: title)).toList(),
      ),
    );
  }
}
