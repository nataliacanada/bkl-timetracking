import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class ContactCard extends StatelessWidget {
  const ContactCard({
    required this.title,
    required this.image,
    required this.telephone,
    required this.fax,
    required this.mobile,
    required this.email,
    required this.website,
  });

  final String title;
  final String image;
  final String telephone;
  final String fax;
  final String mobile;
  final String email;
  final String website;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(
        left: 16,
        bottom: 37,
      ),
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: textTheme.headline1),
          VerticalSpace(16),
          Row(
            children: [
              SizedBox(
                child: Image.asset(image),
                width: 60,
                height: 60,
              ),
              Container(
                margin: const EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('$lblTelephone: $telephone', style: textTheme.headline2),
                    VerticalSpace(2),
                    Text('$lblFax: $fax', style: textTheme.headline2),
                    Text('$lblMobile: $mobile', style: textTheme.headline2),
                    Text('$lblEmail: $email', style: textTheme.headline2),
                    VerticalSpace(17),
                    Text(
                      website,
                      style: textTheme.subtitle1!.copyWith(
                        color: textLinkBlue,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
