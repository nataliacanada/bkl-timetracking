import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class AddressCard extends StatelessWidget {
  const AddressCard({
    required this.title,
    required this.image,
    required this.name,
    required this.street,
    required this.city,
  });

  final String title;
  final String image;
  final String name;
  final String street;
  final String city;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(
        left: 16,
        bottom: 37,
      ),
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: textTheme.headline1),
          VerticalSpace(16),
          Row(
            children: [
              SizedBox(
                child: Image.asset(image),
                width: 60,
                height: 60,
              ),
              Container(
                margin: const EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(name, style: textTheme.headline2),
                    VerticalSpace(2),
                    Text(street, style: textTheme.subtitle1!.copyWith(color: textGrayed)),
                    Text(city, style: textTheme.subtitle1!.copyWith(color: textGrayed)),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
