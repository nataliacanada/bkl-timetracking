import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  CustomAppBar({
    required this.leading,
    this.leadingBG,
    this.title,
    this.subtitle,
    this.trailing,
    required this.bgColor,
    required this.fontColor,
    required this.routeTo,
  });

  final String leading;
  final Color? leadingBG;
  final String? title;
  final String? subtitle;
  final Widget? trailing;
  final Color bgColor;
  final Color fontColor;
  final String routeTo;

  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    final customMenuBtn = IconButton(
        icon: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: leadingBG != null ? leadingBG : null,
          ),
          child: Image.asset(
            leading,
            width: 42,
            height: 42,
          ),
        ),
        onPressed: () {
          if (routeTo.isNotEmpty) {
            Navigator.pushNamed(context, routeTo);
          } else {
            Navigator.pop(context);
          }
        });

    return AppBar(
      backgroundColor: bgColor,
      elevation: 0,
      leadingWidth: 75,
      leading: customMenuBtn,
      titleSpacing: 1,
      actions: [
        if (trailing != null)
          Container(
            margin: const EdgeInsets.only(right: 40),
            child: trailing,
          ),
      ],
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title ?? '',
            style: textTheme.headline1!.copyWith(
              color: fontColor,
              letterSpacing: 0,
            ),
          ),
          if (subtitle != null)
            Text(
              subtitle ?? '',
              style: textTheme.headline2!.copyWith(
                fontSize: 12,
                color: fontColor,
                letterSpacing: 0,
              ),
            ),
        ],
      ),
    );
  }
}
