import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class MyAccountCard extends StatelessWidget {
  const MyAccountCard({
    required this.title,
    required this.image,
    required this.name,
    required this.email,
    required this.role,
  });

  final String title;
  final String image;
  final String name;
  final String email;
  final String role;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(
        left: 16,
        top: 32,
        bottom: 36,
      ),
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: textTheme.headline1),
          VerticalSpace(16),
          Row(
            children: [
              SizedBox(
                child: Image.asset(image),
                width: 70,
                height: 70,
              ),
              Container(
                margin: const EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(name, style: textTheme.headline2),
                    VerticalSpace(2),
                    Text(
                      email,
                      style: textTheme.subtitle1!.copyWith(decoration: TextDecoration.underline),
                    ),
                    VerticalSpace(8),
                    Text(role, style: textTheme.subtitle1),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
