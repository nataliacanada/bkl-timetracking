import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';
import 'package:time_tracking_app/widgets/custom_shape_clipper.dart';

class CustomTextBG extends StatelessWidget {
  CustomTextBG({
    required this.width,
    required this.text,
  });

  final double width;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 9.5),
      child: Row(
        children: [
          Container(
            width: 3,
            height: 21,
            color: appThemePurple,
          ),
          Stack(
            children: [
              ClipPath(
                child: Container(
                  width: width,
                  height: 21,
                  color: Colors.black,
                ),
                clipper: CustomShapeClipper(),
              ),
              Positioned(
                left: 7,
                top: 2,
                child: Text(
                  text,
                  style: textTheme.headline2!.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
