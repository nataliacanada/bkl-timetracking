import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/card_title.dart';
import 'package:time_tracking_app/widgets/time_picker.dart';

class TimePickerCard extends StatefulWidget {
  TimePickerCard({
    required this.title,
    required this.borderColor,
  });

  final String title;
  final Color borderColor;

  @override
  _TimePickerCardState createState() => _TimePickerCardState();
}

class _TimePickerCardState extends State<TimePickerCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(bottom: 32),
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CardTitle(title: widget.title),
          VerticalSpace(16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TimePicker(
                subtitle: start,
                borderColor: widget.borderColor,
              ),
              TimePicker(
                subtitle: end,
                borderColor: widget.borderColor,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
