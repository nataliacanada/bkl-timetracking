import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class CustomButton extends StatelessWidget {
  CustomButton({
    required this.title,
    required this.width,
    required this.height,
    required this.routeTo,
  });

  final String title;
  final double width;
  final double height;
  final String routeTo;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: width,
        height: height,
        color: Colors.black,
        padding: const EdgeInsets.fromLTRB(10, 12, 10, 12),
        child: Row(
          children: [
            Text(title, style: textTheme.headline3!.copyWith(color: Colors.white)),
            Container(
              margin: EdgeInsets.only(left: 13),
              child: Image.asset(
                'assets/icons/send_small.png',
                width: 18,
                height: 18,
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        if (routeTo.isNotEmpty) {
          Navigator.pushNamed(context, routeTo);
        }
      },
    );
  }
}
