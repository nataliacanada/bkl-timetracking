import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';
import 'package:time_tracking_app/widgets/custom_button.dart';

class CommentCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      child: Column(
        children: [
          TextField(
            keyboardType: TextInputType.multiline,
            textCapitalization: TextCapitalization.sentences,
            maxLines: 5,
            cursorHeight: 20,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(20),
              fillColor: grayBG,
              filled: true,
              border: InputBorder.none,
              prefixIcon: Padding(
                padding: const EdgeInsets.only(
                  left: 10,
                  bottom: 40,
                  right: 10,
                ),
                child: Image.asset(
                  'assets/images/profile_avatar.png',
                  width: 70,
                  height: 70,
                ),
              ),
              hintText: addComment,
              hintStyle: textTheme.subtitle1?.copyWith(
                fontSize: 16,
                fontStyle: FontStyle.italic,
                color: Colors.black45,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 32),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {},
                  child: Container(
                    child: Text(cancel),
                  ),
                ),
                HorizontalSpace(25),
                CustomButton(
                  title: save,
                  routeTo: '/add-time-tracking',
                  width: 115,
                  height: 42,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
