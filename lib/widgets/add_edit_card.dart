import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';
import 'package:time_tracking_app/widgets/add_time_button.dart';
import 'package:time_tracking_app/widgets/card_title.dart';
import 'package:time_tracking_app/widgets/custom_tag.dart';

class AddOrEditCard extends StatelessWidget {
  AddOrEditCard({
    required this.title,
    required this.btnColor,
    this.tag,
    this.tagWidth,
    this.routeTo,
    required this.category,
    required this.projectId,
  });

  final String title;
  final Color btnColor;
  final String? tag;
  final double? tagWidth;
  final String? routeTo;
  final String category;
  final String projectId;

  @override
  Widget build(BuildContext context) {
    final addBtn = AddTimeButton(
      btnColor: btnColor,
      routeTo: routeTo,
      category: category,
      projectId: projectId,
    );

    List<Widget> _showActionButtons(String? name) {
      if (name != null) {
        return [
          InkWell(
            child: Image.asset(
              'assets/icons/edit_grey.png',
              width: 20,
              height: 20,
            ),
            onTap: () {},
          ),
          HorizontalSpace(20),
          addBtn,
        ];
      }
      return [addBtn];
    }

    return Card(
      margin: const EdgeInsets.only(bottom: 32),
      elevation: 0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CardTitle(title: title),
              VerticalSpace(6),
              if (tag != null)
                CustomTag(
                  tagTitle: tag ?? '',
                  width: tagWidth as double,
                  bgColor: btnColor,
                ),
              if (tag == null)
                Text(
                  addOrEdit,
                  style: textTheme.headline3!.copyWith(color: textDarkerGray),
                ),
            ],
          ),
          Row(children: _showActionButtons(tag))
        ],
      ),
    );
  }
}
