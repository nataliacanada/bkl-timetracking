import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/pause_page.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/standby_time_page.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/waiting_period_page.dart';

class AddTimeButton extends StatelessWidget {
  const AddTimeButton({
    Key? key,
    required this.btnColor,
    required this.routeTo,
    required this.category,
    required this.projectId,
  }) : super(key: key);

  final Color btnColor;
  final String? routeTo;
  final String category;
  final String projectId;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: 42,
        height: 42,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: btnColor,
        ),
        child: Image.asset('assets/icons/add_white.png'),
      ),
      onTap: () async {
        if (routeTo != null) {
          switch (routeTo) {
            case '/pause':
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PausePage(
                    category: category,
                    projectId: projectId,
                  ),
                ),
              );
              break;
            case '/waiting-period':
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WaitingPeriodPage(),
                ),
              );
              break;
            case '/standby-time':
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => StandbyPage(
                    category: category,
                    projectId: projectId,
                  ),
                ),
              );
              break;
          }
        }
      },
    );
  }
}
