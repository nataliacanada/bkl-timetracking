import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class TimePicker extends StatefulWidget {
  TimePicker({
    required this.subtitle,
    required this.borderColor,
  });

  final String subtitle;
  final Color borderColor;

  @override
  _TimePickerState createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 165,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: widget.borderColor,
            width: 4.1,
          ),
        ),
      ),
      child: Stack(
        children: [
          TimePickerSpinner(
            is24HourMode: true,
            minutesInterval: 15,
            normalTextStyle: textTheme.headline2!.copyWith(
              fontSize: 14,
              color: Colors.grey.shade300,
              letterSpacing: 0,
            ),
            highlightedTextStyle: textTheme.headline2!.copyWith(letterSpacing: 0),
            itemHeight: 40,
            itemWidth: 30,
            spacing: 1,
            isForce2Digits: true,
            onTimeChange: (time) {
              setState(() {});
            },
          ),
          Positioned(
            top: 51.9,
            left: 28,
            child: Image.asset(
              'assets/icons/picker.png',
              width: 18,
              height: 18,
            ),
          ),
          Positioned(
            top: 50.8,
            left: 85,
            child: Text(
              ':',
              style: TextStyle(
                fontSize: 15,
              ),
            ),
          ),
          Positioned(
            top: 70,
            left: 60,
            child: Text(
              widget.subtitle,
              style: textTheme.headline2!.copyWith(
                fontSize: 12,
                color: Colors.grey.shade500,
                letterSpacing: 0,
              ),
            ),
          ),
          Positioned(
            top: 51.3,
            left: 116,
            child: Text(
              clockPeriod,
              style: textTheme.headline2!.copyWith(
                fontSize: 15,
                letterSpacing: 0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
