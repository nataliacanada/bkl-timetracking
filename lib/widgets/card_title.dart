import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class CardTitle extends StatelessWidget {
  CardTitle({required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(title, style: textTheme.headline1);
  }
}
