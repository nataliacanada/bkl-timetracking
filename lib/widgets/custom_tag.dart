import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class CustomTag extends StatelessWidget {
  CustomTag({
    required this.tagTitle,
    required this.width,
    required this.bgColor,
  });

  final String tagTitle;
  final double width;
  final Color bgColor;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: width,
        height: 32,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50)),
          color: bgColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              tagTitle,
              style: textTheme.subtitle1!.copyWith(
                fontSize: 14,
                color: Colors.white,
              ),
            ),
            Image.asset(
              'assets/icons/close_white.png',
              width: 20,
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
