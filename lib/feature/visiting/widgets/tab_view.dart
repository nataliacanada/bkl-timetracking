import 'package:flutter/material.dart';
import 'package:time_tracking_app/widgets/address_card.dart';
import 'package:time_tracking_app/widgets/contact_card.dart';
import 'package:time_tracking_app/widgets/my_account_card.dart';

class TabView extends StatelessWidget {
  TabView({
    required this.myAccountCard,
    required this.imageQR,
    required this.addressCard,
    required this.contactCard,
  });

  final MyAccountCard myAccountCard;
  final String imageQR;
  final AddressCard addressCard;
  final ContactCard contactCard;

  @override
  Widget build(BuildContext context) {
    Widget codeQR = Container(
      margin: const EdgeInsets.only(bottom: 36),
      child: Image.asset(
        imageQR,
        width: 240,
        height: 240,
      ),
    );

    return Container(
      color: Colors.white,
      margin: const EdgeInsets.only(
        top: 16,
        left: 16,
        right: 16,
      ),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              myAccountCard,
              codeQR,
              addressCard,
              contactCard,
            ],
          ),
        ),
      ),
    );
  }
}
