import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/visiting/widgets/tab_view.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/address_card.dart';
import 'package:time_tracking_app/widgets/contact_card.dart';
import 'package:time_tracking_app/widgets/custom_appbar.dart';
import 'package:time_tracking_app/widgets/custom_tabbar.dart';
import 'package:time_tracking_app/widgets/my_account_card.dart';

class VisitingPage extends StatefulWidget {
  @override
  _VisitingPageState createState() => _VisitingPageState();
}

class _VisitingPageState extends State<VisitingPage> with SingleTickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(
      length: 2,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: grayBG,
      appBar: CustomAppBar(
        leading: 'assets/icons/menu.png',
        bgColor: Colors.white,
        fontColor: Colors.black,
        routeTo: '/side-menu',
      ),
      body: Column(
        children: [
          CustomTabBar(
            tabTitles: [myBusinessCard, supervisor],
            controller: _controller,
          ),
          Expanded(
            child: TabBarView(
              controller: _controller,
              children: [
                // Meine Visitenkarte TabView
                TabView(
                  myAccountCard: MyAccountCard(
                    title: businessCard,
                    image: 'assets/images/profile_avatar.png',
                    name: 'Greg Neu',
                    email: 'greg.neu@f-bootcamp.com',
                    role: assembler,
                  ),
                  imageQR: 'assets/images/business_qr.png',
                  addressCard: AddressCard(
                    title: address,
                    image: 'assets/icons/address.png',
                    name: 'Flutter Bootcamp',
                    street: '6783 Ayala Ave',
                    city: '1200 Metro Manila',
                  ),
                  contactCard: ContactCard(
                    title: contact,
                    image: 'assets/icons/user_kontakt.png',
                    telephone: '+49 1234 56 789 01',
                    fax: '+49 1234 56 789 01-2',
                    mobile: '+49 1234 56',
                    email: 'greg.neu@f-bootcamp.com',
                    website: 'www.flutter-bootcamp.com',
                  ),
                ),
                // Vorgesetzte TabView
                TabView(
                  myAccountCard: MyAccountCard(
                    title: supervisor,
                    image: 'assets/images/profile_avatar.png',
                    name: 'Andero Mustermann',
                    email: 'andero.mustermann@f-bootcamp.com',
                    role: departmentManager,
                  ),
                  imageQR: 'assets/images/superior_qr.png',
                  addressCard: AddressCard(
                    title: address,
                    image: 'assets/icons/address.png',
                    name: 'Flutter Bootcamp',
                    street: '6783 Ayala Ave',
                    city: '1200 Metro Manila',
                  ),
                  contactCard: ContactCard(
                    title: contact,
                    image: 'assets/icons/user_kontakt.png',
                    telephone: '+49 1234 56 789 01',
                    fax: '+49 1234 56 789 01-2',
                    mobile: '+49 1234 56',
                    email: 'andreo.mustermann@f-bo...',
                    website: 'www.flutter-bootcamp.com',
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
