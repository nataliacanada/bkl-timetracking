import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class MenuItem extends StatefulWidget {
  MenuItem({
    required this.icon,
    required this.title,
    required this.isActive,
    required this.routeTo,
  });

  final String icon;
  final String title;
  final bool isActive;
  final String routeTo;

  @override
  _MenuItemState createState() => _MenuItemState();
}

class _MenuItemState extends State<MenuItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: 130,
        margin: const EdgeInsets.only(left: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 15.61),
              child: Image.asset(
                widget.icon,
                color: inactiveIconGray,
                width: 50,
                height: 50,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 36),
              child: Column(
                children: [
                  Text(
                    widget.title,
                    style: textTheme.headline3!.copyWith(
                      color: widget.isActive == true ? Colors.black : textGrayed,
                    ),
                  ),
                  if (widget.isActive)
                    Container(
                      width: 93,
                      height: 4,
                      margin: const EdgeInsets.only(top: 8),
                      color: appThemePurple,
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        if (widget.routeTo.isNotEmpty) {
          Navigator.popAndPushNamed(context, widget.routeTo);
        }
      },
    );
  }
}
