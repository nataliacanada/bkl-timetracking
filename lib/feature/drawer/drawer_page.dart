import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/drawer/widgets/menu_item.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/custom_appbar.dart';

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomAppBar(
              leading: 'assets/icons/close.png',
              bgColor: Colors.white,
              fontColor: Colors.black,
              trailing: Image.asset(
                'assets/images/flutter_logo.png',
                width: 19.51,
                height: 31.91,
              ),
              routeTo: '',
            ),
            SizedBox(height: 20),
            MenuItem(
              icon: 'assets/icons/user.png',
              title: myAccount,
              isActive: false,
              routeTo: '/my-account',
            ),
            MenuItem(
              icon: 'assets/icons/visiting_card.png',
              title: businessCard,
              isActive: false,
              routeTo: '/visiting',
            ),
            MenuItem(
              icon: 'assets/icons/time_tracking.png',
              title: timeRecording,
              isActive: true,
              routeTo: '/time-tracking',
            ),
            MenuItem(
              icon: 'assets/icons/stakes.png',
              title: myStakes,
              isActive: false,
              routeTo: '',
            ),
          ],
        ),
      ),
    );
  }
}
