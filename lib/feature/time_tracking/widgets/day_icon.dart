import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class DayIcon extends StatelessWidget {
  DayIcon({
    required this.iconPath,
    required this.color,
    required this.day,
  });

  final String? iconPath;
  final Color color;
  final String day;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 8),
      child: Column(
        children: [
          Container(
            width: 42,
            height: 42,
            margin: const EdgeInsets.only(bottom: 2),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: color,
            ),
            child: iconPath != null
                ? Image.asset(iconPath!)
                : Center(
                    child: Text(day, style: textTheme.headline1!.copyWith(color: Colors.black12)),
                  ),
          ),
          Text(day, style: textTheme.subtitle1!.copyWith(fontFamily: 'Allerta-Stencil')),
        ],
      ),
    );
  }
}
