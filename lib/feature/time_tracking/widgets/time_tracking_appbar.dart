import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/time_tracking/widgets/day_icon.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';
import 'package:time_tracking_app/widgets/custom_icon_button.dart';
import 'package:time_tracking_app/widgets/custom_text_bg.dart';

class TimeTrackingAppBar extends StatelessWidget implements PreferredSizeWidget {
  TimeTrackingAppBar({
    required this.leading,
    required this.title,
    required this.subtitles,
    required this.calendarBtnTrailingIconPath,
    required this.addBtnTrailingIconPath,
    required this.bgColor,
    required this.leadingRoute,
  });

  final String leading;
  final String title;
  final List<String> subtitles;
  final String calendarBtnTrailingIconPath;
  final String addBtnTrailingIconPath;
  final Color bgColor;
  final String leadingRoute;

  @override
  Size get preferredSize => const Size.fromHeight(140);

  @override
  Widget build(BuildContext context) {
    final menuBtn = IconButton(
      icon: Image.asset(leading),
      onPressed: () => Navigator.pushNamed(context, leadingRoute),
    );

    final appBarTitle = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: textTheme.headline1!.copyWith(color: Colors.black)),
          Row(
            children: [
              CustomTextBG(width: 60, text: subtitles[0]),
              Text(subtitles[1], style: textTheme.subtitle1!.copyWith(color: textGrayed)),
            ],
          ),
        ],
      ),
    );

    return AppBar(
      elevation: 0,
      leadingWidth: 75,
      leading: menuBtn,
      titleSpacing: 1,
      title: appBarTitle,
      backgroundColor: Colors.white,
      actions: [
        CustomIconButton(
          iconPath: calendarBtnTrailingIconPath,
          withBackground: false,
        ),
        CustomIconButton(
          iconPath: addBtnTrailingIconPath,
          withBackground: true,
          color: grayBorder,
        )
      ],
      flexibleSpace: Stack(
        children: [
          Positioned(
            bottom: 5,
            left: 18,
            right: 18,
            child: Container(
              height: 60,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  DayIcon(
                    color: appColorRed,
                    iconPath: 'assets/icons/check_small.png',
                    day: mon,
                  ),
                  DayIcon(
                    color: appColorDirtyYellow,
                    iconPath: 'assets/icons/check_small.png',
                    day: tue,
                  ),
                  DayIcon(
                    color: appThemeBlue,
                    iconPath: 'assets/icons/send_small.png',
                    day: wed,
                  ),
                  DayIcon(
                    color: appThemePurple,
                    iconPath: 'assets/icons/edit_small.png',
                    day: thur,
                  ),
                  DayIcon(
                    color: Colors.grey.shade100,
                    iconPath: null,
                    day: fri,
                  ),
                  DayIcon(
                    color: Colors.grey.shade100,
                    iconPath: null,
                    day: sat,
                  ),
                  DayIcon(
                    color: Colors.grey.shade100,
                    iconPath: null,
                    day: sun,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
