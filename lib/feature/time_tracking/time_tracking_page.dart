// @dart=2.9
import 'package:flutter/material.dart';
import 'package:time_machine/time_machine.dart';
import 'package:time_tracking_app/feature/time_tracking/widgets/time_tracking_appbar.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:timetable/timetable.dart';

class TimeTrackingPage extends StatefulWidget {
  @override
  _TimeTrackingPageState createState() => _TimeTrackingPageState();
}

class _TimeTrackingPageState extends State<TimeTrackingPage> {
  TimetableController<BasicEvent> myController;

  final myEventProvider = EventProvider.list(
    [
      BasicEvent(
        id: 0,
        title: '$project 123',
        color: Colors.blue,
        start: LocalDate.today().at(LocalTime(13, 0, 0)),
        end: LocalDate.today().at(LocalTime(15, 0, 0)),
      ),
    ],
  );

  @override
  void initState() {
    myController = TimetableController<BasicEvent>(
      eventProvider: myEventProvider,
      // Optional parameters with their default values:
      initialTimeRange: InitialTimeRange.range(
        startTime: LocalTime(8, 0, 0),
        endTime: LocalTime(20, 0, 0),
      ),
      initialDate: LocalDate.today(),
      visibleRange: VisibleRange.week(),
      firstDayOfWeek: DayOfWeek.monday,
    );
    super.initState();
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TimeTrackingAppBar(
        leading: 'assets/icons/menu.png',
        title: thursday,
        subtitles: [open, '12.01.2021'],
        calendarBtnTrailingIconPath: 'assets/icons/calendar_button.png',
        addBtnTrailingIconPath: 'assets/icons/add_black.png',
        bgColor: Colors.white,
        leadingRoute: '/side-menu',
      ),
      body: Timetable<BasicEvent>(
        controller: myController,
        eventBuilder: (event) => BasicEventWidget(event),
        allDayEventBuilder: (context, event, info) => BasicAllDayEventWidget(event, info: info),
      ),
    );
  }
}
