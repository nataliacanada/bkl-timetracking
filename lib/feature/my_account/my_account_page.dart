import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/my_account/widgets/myaccount_view.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/widgets/custom_appbar.dart';

class MyAccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: grayBG,
      appBar: CustomAppBar(
        leading: 'assets/icons/menu.png',
        bgColor: Colors.white,
        fontColor: Colors.black,
        routeTo: '/side-menu',
      ),
      body: MyAccountView(),
    );
  }
}
