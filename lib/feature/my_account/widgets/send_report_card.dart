import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';
import 'package:time_tracking_app/widgets/custom_button.dart';

class SendReportCard extends StatelessWidget {
  SendReportCard({
    required this.title,
    required this.date,
    required this.btnTitle,
  });

  final String title;
  final String date;
  final String btnTitle;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(
        left: 16,
        bottom: 32,
      ),
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: textTheme.headline1),
          VerticalSpace(18),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                child: Image.asset('assets/icons/kalender.png'),
                width: 60,
                height: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25.88),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(date, style: textTheme.headline2!.copyWith(letterSpacing: -1.2)),
                    VerticalSpace(16),
                    CustomButton(
                      title: btnTitle,
                      routeTo: '',
                      width: 220,
                      height: 42,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
