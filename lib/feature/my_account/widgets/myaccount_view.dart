import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/my_account/widgets/contact_person.dart';
import 'package:time_tracking_app/feature/my_account/widgets/send_report_card.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/my_account_card.dart';

class MyAccountView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: const EdgeInsets.only(
        top: 16,
        left: 16,
        right: 16,
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            MyAccountCard(
              title: myAccount,
              image: 'assets/images/profile_avatar.png',
              name: 'Greg Neu',
              email: 'max.mustermann@bkl.de',
              role: assembler,
            ),
            ContactPersonCard(
              image: 'assets/images/contact_avatar.png',
              name: 'Ingo Flamingo',
              email: 'ingo.flamingo@f-bootcamp.com',
              number: '0160 - 123456789',
            ),
            SendReportCard(
              title: weeklyReport,
              date: '12.03 - 19.03.2021',
              btnTitle: sendWeeklyReport,
            ),
            SendReportCard(
              title: monthlyReport,
              date: 'April 2020',
              btnTitle: createMonthlyReport,
            ),
          ],
        ),
      ),
    );
  }
}
