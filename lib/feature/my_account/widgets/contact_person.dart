import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';
import 'package:time_tracking_app/widgets/custom_text_bg.dart';

class ContactPersonCard extends StatelessWidget {
  const ContactPersonCard({
    required this.image,
    required this.name,
    required this.email,
    required this.number,
  });

  final String image;
  final String name;
  final String email;
  final String number;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(
        left: 16,
        bottom: 32,
      ),
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(contactPerson, style: textTheme.headline1),
          VerticalSpace(16),
          Row(
            children: [
              SizedBox(
                child: Image.asset(image),
                width: 70,
                height: 70,
              ),
              Container(
                margin: const EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(name, style: textTheme.headline2),
                    VerticalSpace(2),
                    Text(email, style: textTheme.subtitle1),
                    VerticalSpace(8),
                    CustomTextBG(width: 130, text: number),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
