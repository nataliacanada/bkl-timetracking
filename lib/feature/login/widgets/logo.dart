import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  Logo({required this.image});

  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 163),
      child: Center(
        child: Image.asset(
          image,
          width: 70,
          height: 70,
        ),
      ),
    );
  }
}
