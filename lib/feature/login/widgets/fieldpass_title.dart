import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class FieldPassTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 363,
      height: 51,
      margin: const EdgeInsets.only(
        top: 48,
        bottom: 56,
      ),
      child: Center(child: Text(flutterFieldPass, style: textTheme.headline3)),
    );
  }
}
