import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class Footer extends StatelessWidget {
  Footer({
    required this.title1,
    required this.title2,
  });

  final String title1;
  final String title2;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 282),
      width: 188,
      height: 16,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title1, style: textTheme.subtitle1!.copyWith(fontSize: 14)),
          HorizontalSpace(32),
          Text(title2, style: textTheme.subtitle1!.copyWith(fontSize: 14)),
        ],
      ),
    );
  }
}
