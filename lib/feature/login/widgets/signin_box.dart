import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class SignInBox extends StatelessWidget {
  SignInBox({
    required this.icon,
    required this.title,
  });

  final String icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: 302,
        height: 57,
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  padding: const EdgeInsets.only(
                    left: 15,
                    top: 16,
                  ),
                  child: Image.asset(icon),
                ),
                Container(
                  padding: const EdgeInsets.only(
                    left: 13,
                    top: 20,
                  ),
                  child: Text(title, style: textTheme.subtitle1!.copyWith(fontSize: 14)),
                ),
                Container(
                  padding: const EdgeInsets.only(
                    left: 68,
                    top: 20,
                  ),
                  child: Image.asset('assets/icons/four_arrow.png'),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 13),
              width: 302,
              height: 3,
              color: grayBorder,
            ),
          ],
        ),
      ),
      onTap: () => Navigator.pushNamed(context, '/my-account'),
    );
  }
}
