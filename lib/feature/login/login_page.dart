import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/login/widgets/fieldpass_title.dart';
import 'package:time_tracking_app/feature/login/widgets/footer.dart';
import 'package:time_tracking_app/feature/login/widgets/logo.dart';
import 'package:time_tracking_app/feature/login/widgets/signin_box.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Logo(image: 'assets/images/flutter_logo.png'),
            FieldPassTitle(),
            SignInBox(
              icon: 'assets/icons/microsoft_logo.png',
              title: signInWithMicrosoft,
            ),
            Footer(
              title1: imprint,
              title2: privacy,
            ),
          ],
        ),
      ),
    );
  }
}
