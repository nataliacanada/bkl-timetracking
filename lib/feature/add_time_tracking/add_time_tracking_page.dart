import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/pause_tab.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/working_time_tab.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/custom_appbar.dart';
import 'package:time_tracking_app/widgets/custom_tabbar.dart';

class AddTimeTrackingPage extends StatefulWidget {
  @override
  _AddTimeTrackingPageState createState() => _AddTimeTrackingPageState();
}

class _AddTimeTrackingPageState extends State<AddTimeTrackingPage> with SingleTickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        leading: 'assets/icons/close.png',
        bgColor: Colors.white,
        fontColor: Colors.black,
        trailing: Image.asset(
          'assets/images/flutter_logo.png',
          width: 23.94,
          height: 39.89,
        ),
        routeTo: '/time-tracking',
      ),
      body: Column(
        children: [
          CustomTabBar(
            tabTitles: [workingTime, breakTime],
            controller: _controller,
          ),
          Expanded(
            child: TabBarView(
              controller: _controller,
              children: [
                // Arbeitszeit TabView
                WorkingTimeTab(),
                // Pause TabView
                PauseTab(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
