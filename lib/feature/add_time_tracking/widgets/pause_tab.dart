import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/list_constants.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';
import 'package:time_tracking_app/widgets/card_title.dart';
import 'package:time_tracking_app/widgets/comment_card.dart';
import 'package:time_tracking_app/widgets/time_picker_card.dart';

class PauseTab extends StatefulWidget {
  @override
  _PauseTabState createState() => _PauseTabState();
}

class _PauseTabState extends State<PauseTab> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 25,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CardTitle(title: category),
            DropdownButtonFormField(
              icon: Image.asset('assets/icons/arrow_down.png'),
              isExpanded: true,
              itemHeight: 68,
              decoration: InputDecoration(
                hintText: pleaseSelectCategory,
                hintStyle: textTheme.subtitle1!.copyWith(
                  fontSize: 14,
                  color: textDarkerGray,
                ),
              ),
              items: categories
                  .map((category) => DropdownMenuItem(
                        value: category,
                        child: Text(category),
                      ))
                  .toList(),
              onChanged: (value) => setState(() {}),
            ),
            VerticalSpace(20),
            TimePickerCard(
              title: breakTime,
              borderColor: appThemeBlue,
            ),
            CommentCard(),
          ],
        ),
      ),
    );
  }
}
