import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/list_constants.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/comment_card.dart';
import 'package:time_tracking_app/widgets/custom_appbar.dart';
import 'package:time_tracking_app/widgets/time_picker_card.dart';

class PausePage extends StatefulWidget {
  PausePage({
    this.category,
    this.projectId,
  });

  final String? category;
  final String? projectId;

  @override
  _PausePageState createState() => _PausePageState();
}

class _PausePageState extends State<PausePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        leading: 'assets/icons/arrow_back.png',
        bgColor: appThemeBlue,
        title: breakTime,
        subtitle: '$project: ${widget.projectId}',
        fontColor: Colors.white,
        routeTo: '/add-time-tracking',
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 25,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownButtonFormField(
                icon: Image.asset('assets/icons/arrow_down.png'),
                isExpanded: true,
                itemHeight: 68,
                value: widget.category,
                items: categories
                    .map((category) => DropdownMenuItem(
                          value: category,
                          child: Text(category),
                        ))
                    .toList(),
                onChanged: (value) => setState(() {}),
              ),
              VerticalSpace(20),
              TimePickerCard(
                title: breakTime,
                borderColor: appThemeBlue,
              ),
              CommentCard(),
            ],
          ),
        ),
      ),
    );
  }
}
