import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/add_edit_card.dart';
import 'package:time_tracking_app/widgets/comment_card.dart';
import 'package:time_tracking_app/widgets/time_picker_card.dart';

class ShowContent extends StatelessWidget {
  ShowContent({
    required this.isBothSelected,
    required this.selectedCategory,
    required this.selectedProjectId,
  });

  final bool isBothSelected;
  final String selectedCategory;
  final String selectedProjectId;

  @override
  Widget build(BuildContext context) {
    if (!isBothSelected)
      return Column(
        children: [
          AddOrEditCard(
            title: employees,
            btnColor: Colors.black,
            category: selectedCategory,
            projectId: selectedProjectId,
          ),
          TimePickerCard(
            title: workingTime,
            borderColor: appThemeLightBlue,
          ),
          CommentCard(),
        ],
      );
    else
      return Column(
        children: [
          AddOrEditCard(
            title: employees,
            btnColor: Colors.black,
            tag: 'Greg Neu',
            tagWidth: 130,
            routeTo: '/pause',
            category: selectedCategory,
            projectId: selectedProjectId,
          ),
          TimePickerCard(
            title: workingTime,
            borderColor: appThemeLightBlue,
          ),
          AddOrEditCard(
            title: breakTime,
            btnColor: appThemeBlue,
            routeTo: '/pause',
            category: selectedCategory,
            projectId: selectedProjectId,
          ),
          // WaitingPeriod Add
          AddOrEditCard(
            title: waitingTime,
            btnColor: appThemeYellow,
            category: selectedCategory,
            projectId: selectedProjectId,
          ),
          // StandbyTime Add
          AddOrEditCard(
            title: standbyTime,
            btnColor: appThemePurple,
            tag: '08:00 $clockPeriod - 08 : 30 $clockPeriod',
            tagWidth: 197,
            routeTo: '/standby-time',
            category: selectedCategory,
            projectId: selectedProjectId,
          ),
          CommentCard(),
        ],
      );
  }
}
