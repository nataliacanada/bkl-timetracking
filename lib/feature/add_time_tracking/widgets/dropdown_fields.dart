import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/card_title.dart';

class DropDownFields extends StatelessWidget {
  const DropDownFields({
    required this.dropdownCategory,
    required this.dropdownProjectId,
    Key? key,
  }) : super(key: key);

  final DropdownButtonFormField dropdownCategory;
  final DropdownButtonFormField dropdownProjectId;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CardTitle(title: category),
        dropdownCategory,
        VerticalSpace(20),
        CardTitle(title: projectNumber),
        dropdownProjectId,
        VerticalSpace(30),
      ],
    );
  }
}
