import 'package:flutter/material.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/dropdown_fields.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/show_content.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/list_constants.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/utilities/text_theme.dart';

class WorkingTimeTab extends StatefulWidget {
  WorkingTimeTab({
    this.category,
    this.projectId,
  });

  final String? category;
  final String? projectId;

  @override
  _WorkingTimeTabState createState() => _WorkingTimeTabState();
}

class _WorkingTimeTabState extends State<WorkingTimeTab> {
  String selectedCategory = '';
  String selectedProjectId = '';

  bool isCategorySelected = false;
  bool isProjectIdSelected = false;

  @override
  Widget build(BuildContext context) {
    final projectIds = ['1298721398']
        .map((projectId) => DropdownMenuItem(
              value: projectId,
              child: Text(projectId),
            ))
        .toList();

    void setProjectCategory(String value) {
      setState(() {
        selectedCategory = value;
        isCategorySelected = true;
      });
    }

    void setProjectID(String value) {
      setState(() {
        selectedProjectId = value;
        isProjectIdSelected = true;
      });
    }

    final dropdownCategory = DropdownButtonFormField(
      icon: Image.asset('assets/icons/arrow_down.png'),
      isExpanded: true,
      itemHeight: 80,
      decoration: InputDecoration(
        hintText: pleaseSelectCategory,
        hintStyle: textTheme.subtitle1!.copyWith(
          fontSize: 14,
          color: textDarkerGray,
        ),
      ),
      items: categories
          .map((category) => DropdownMenuItem(
                value: category,
                child: Text(category),
              ))
          .toList(),
      onChanged: (value) => setProjectCategory(value as String),
    );

    final dropdownProjectId = DropdownButtonFormField(
      icon: Image.asset('assets/icons/arrow_down.png'),
      isExpanded: true,
      itemHeight: 68,
      decoration: InputDecoration(
        hintText: addProjectNumber,
        hintStyle: textTheme.subtitle1!.copyWith(
          fontSize: 14,
          color: textDarkerGray,
        ),
      ),
      items: projectIds,
      onChanged: (value) => setProjectID(value as String),
    );

    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 25,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DropDownFields(
              dropdownCategory: dropdownCategory,
              dropdownProjectId: dropdownProjectId,
            ),
            ShowContent(
              isBothSelected: isCategorySelected && isProjectIdSelected,
              selectedCategory: selectedCategory,
              selectedProjectId: selectedProjectId,
            ),
          ],
        ),
      ),
    );
  }
}
