import 'package:flutter/material.dart';
import 'package:time_tracking_app/utilities/color_theme.dart';
import 'package:time_tracking_app/utilities/spacer.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';
import 'package:time_tracking_app/widgets/custom_appbar.dart';

class CalendarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        leading: 'assets/icons/close.png',
        leadingBG: grayBorder,
        title: year,
        bgColor: Colors.white,
        fontColor: Colors.black,
        routeTo: '',
      ),
      body: Container(
        margin: const EdgeInsets.all(20),
        child: Container(
          padding: const EdgeInsets.only(left: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/januar.png',
                      width: 110,
                      height: 140,
                    ),
                    HorizontalSpace(5),
                    Image.asset(
                      'assets/images/februar.png',
                      width: 110,
                      height: 140,
                    ),
                    HorizontalSpace(5),
                    Image.asset(
                      'assets/images/marz.png',
                      width: 110,
                      height: 140,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/april.png',
                      width: 110,
                      height: 140,
                    ),
                    HorizontalSpace(5),
                    Image.asset(
                      'assets/images/mai.png',
                      width: 110,
                      height: 140,
                    ),
                    HorizontalSpace(5),
                    Image.asset(
                      'assets/images/juni.png',
                      width: 110,
                      height: 140,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/juli.png',
                      width: 110,
                      height: 140,
                    ),
                    HorizontalSpace(5),
                    Image.asset(
                      'assets/images/august.png',
                      width: 110,
                      height: 140,
                    ),
                    HorizontalSpace(5),
                    Image.asset(
                      'assets/images/september.png',
                      width: 110,
                      height: 140,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/oktober.png',
                      width: 110,
                      height: 140,
                    ),
                    HorizontalSpace(5),
                    Image.asset(
                      'assets/images/november.png',
                      width: 110,
                      height: 140,
                    ),
                    HorizontalSpace(5),
                    Image.asset(
                      'assets/images/dezember.png',
                      width: 110,
                      height: 140,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
