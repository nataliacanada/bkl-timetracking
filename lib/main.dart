// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:time_machine/time_machine.dart';
import 'package:time_tracking_app/feature/add_time_tracking/add_time_tracking_page.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/pause_page.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/standby_time_page.dart';
import 'package:time_tracking_app/feature/add_time_tracking/widgets/waiting_period_page.dart';
import 'package:time_tracking_app/feature/calendar/calendar_page.dart';
import 'package:time_tracking_app/feature/drawer/drawer_page.dart';
import 'package:time_tracking_app/feature/login/login_page.dart';
import 'package:time_tracking_app/feature/my_account/my_account_page.dart';
import 'package:time_tracking_app/feature/time_tracking/time_tracking_page.dart';
import 'package:time_tracking_app/feature/visiting/visiting_page.dart';
import 'package:time_tracking_app/utilities/string_constants.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await TimeMachine.initialize({
    'rootBundle': rootBundle,
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (_) => LoginScreen(),
        '/my-account': (_) => MyAccountPage(),
        '/side-menu': (_) => DrawerPage(),
        '/visiting': (_) => VisitingPage(),
        '/time-tracking': (_) => TimeTrackingPage(),
        '/calendar': (_) => CalendarPage(),
        '/add-time-tracking': (_) => AddTimeTrackingPage(),
        '/pause': (_) => PausePage(),
        '/waiting-period': (_) => WaitingPeriodPage(),
        '/standby-time': (_) => StandbyPage(),
      },
    );
  }
}
