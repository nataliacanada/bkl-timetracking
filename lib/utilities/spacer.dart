import 'package:flutter/material.dart';

class HorizontalSpace extends StatelessWidget {
  final double width;

  HorizontalSpace(this.width);

  @override
  Widget build(BuildContext context) => SizedBox(width: width);
}

class VerticalSpace extends StatelessWidget {
  final double height;

  VerticalSpace(this.height);

  @override
  Widget build(BuildContext context) => SizedBox(height: height);
}
