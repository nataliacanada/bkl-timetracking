import 'package:flutter/material.dart';

final textTheme = TextTheme(
  headline1: TextStyle(
    fontFamily: 'Allerta-Stencil',
    fontSize: 22,
    letterSpacing: -1.3,
  ),
  headline2: TextStyle(
    fontFamily: 'Allerta-Stencil',
    fontSize: 16,
    letterSpacing: -1.3,
  ),
  headline3: TextStyle(
    fontFamily: 'Roboto',
    fontSize: 14,
  ),
  subtitle1: TextStyle(
    fontFamily: 'Mulish',
    fontSize: 12,
  ),
);
