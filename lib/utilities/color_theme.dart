import 'package:flutter/material.dart';

const appThemeBlue = Color.fromRGBO(103, 136, 255, 1);
const appThemeLightBlue = Color.fromRGBO(76, 183, 229, 1);
const appThemeYellow = Color.fromRGBO(255, 183, 43, 1);
const appThemePurple = Color.fromRGBO(132, 101, 255, 1);
const textGrayed = Color.fromRGBO(25, 29, 38, 0.7);
const inactiveIconGray = Color.fromRGBO(25, 29, 38, 0.2);
const textDarkerGray = Color.fromRGBO(185, 185, 185, 1);
const textLinkBlue = Color.fromRGBO(0, 164, 234, 1);
const grayBorder = Color.fromRGBO(224, 224, 224, 1);
const grayBG = Color.fromRGBO(245, 245, 245, 1);
const appColorRed = Color.fromRGBO(225, 65, 65, 1);
const appColorDirtyYellow = Color.fromRGBO(225, 183, 43, 1);
